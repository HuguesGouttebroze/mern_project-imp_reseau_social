const router = require('express').Router();
const authController = require('../controllers/auth.controller');

// on "/register" uri, start "signUp" fnc
router.post("/register", authController.signUp);

module.exxports = router;