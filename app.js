const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB, 
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log(`Connexion to MongoDB ok.`))
  .catch(() => console.log(`Connexion to MongoDB is failing. No connexion.`))