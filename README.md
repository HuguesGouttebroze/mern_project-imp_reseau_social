# MERN project template from scratch

## Init project backend with nodeJS

## Add dependencies

* add scripts needed, don't use the one to start node
* start node application script cmd with `npm` :
    - in `package.json` file, add into `scripts` a new script that will 
    served to start node server. But we will used `nodemon` package that 
    alow to don't restart our server. So,  we will add `"start"` into the key 
    script, and this key will take in value `"nodemon server"`.

    So, It's give you something with this tast : 

    `"start": "nodemon server"`

                or,

```json
  "scripts": {
    "start": "nodemon server",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
``` 
## Percistance, or connect our application to MongoDB
* We need to create a account to mongo, with a cluster